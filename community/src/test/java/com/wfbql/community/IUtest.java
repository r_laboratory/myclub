package com.wfbql.community;

import com.wfbql.community.dao.InfoDao;
import com.wfbql.community.dao.LogDao;
import com.wfbql.community.daomain.Info;
import com.wfbql.community.daomain.Log;
import com.wfbql.community.daomain.News;
import com.wfbql.community.service.*;
import com.wfbql.community.utils.DateUtil;
import com.wfbql.community.utils.OSSClientUtil;
import com.wfbql.community.utils.UUIDUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.Random;

@SpringBootTest
class IUtest {
    @Autowired
    private IUservice iUservice;

    @Autowired
    private InfoService infoService;

    @Autowired
    private InfoDao infoDao;

    @Autowired
    private LogDao logDao;

    @Autowired
    private LogService logService;

    @Autowired
    private ElasticService elasticService;
    @Autowired
    MailSender mailSenders;

    @Test
    void test12(){
        mailSenders.send("2528697821@qq.com","测试邮件","<h1>测试邮件内容</h1>","","");
    }




    @Test
    void test11(){
        Random random = new Random();
        int i = random.nextInt(10);
        System.out.println(i);
    }

    @Test
    void  test9() throws ParseException {
        Log log = new Log();
        log.setDatee("2017-0-1");
        log.setTheme("这个是张三");
        log.setContent("这个是李四");
        log.setUser_id("1");
        log.setEye(0);
        log.setUsername("王二麻子");
        log.setId(UUIDUtils.getUUID());


        logDao.insertLogById(log);
    }


    @Test
    void test8() throws IOException, ParseException {

        /**
         * public String getDatee() {
         *         String s = DateUtil.date2String(this.datee);
         *         String substring = s.substring(0, 10);
         *         return substring;
         *     }
         */

       // infoDao.updateFace("2e6e724ca06e4b669cb4834fb3991104","wfbql");

        //System.out.println(infoDao.selectInfo("2e6e724ca06e4b669cb4834fb3991104").toString());
        Info info =  new Info();
        info.setId("87e48785b38642f1959c1e520a3256cd");
        info.setAlias("lisi");
        info.setEmile("");
        info.setDatee(DateUtil.time2String("2017-01-06"));
        info.setEvaluation("evaluation");
        infoDao.updateInfo(info);
       // infoDao.updateInfo1("87e48785b38642f1959c1e520a3256cd","lsisi","2017-01-06","dsadas");
    }

    @Test
    void test7() throws IOException {
        List<Map<String, Object>> lists = elasticService.searchPage("java", 2, 10);
        System.out.println(lists.size());
        for (int i = 0; i < lists.size(); i++) {
            System.out.println(lists.get(i).toString());
        }

    }

    @Test
    void test6() throws IOException {
        System.out.println(elasticService.parseContent("java"));
    }

    @Test
    void test5(){
        // System.out.println(logService.getLogById("1").toString());
        String url = new OSSClientUtil().getImgUrl("t9.jpg");
        System.out.println(url);
    }

    @Test
    void test4(){
       // System.out.println(logService.getLogById("1").toString());
        logService.deleteById("3");
    }

    @Test
    void test3(){
        List<Log> logs = logService.getLogs("2e6e724ca06e4b669cb4834fb3991104");
        for (Log log: logs) {
            System.out.println(log.toString());
        }
    }

    @Test
    void test2(){
        System.out.println(infoService.getInfo("87e48785b38642f1959c1e520a3256cd"));
    }

    @Test
    void test1(){
       // System.out.println(iUservice.getNameById("2e6e724ca06e4b669cb4834fb3991104"));
       // System.out.println(iUservice.getImageById("2e6e724ca06e4b669cb4834fb3991104"));
        /*List<Log> recommendLogs = iUservice.getRecommendLogs();
        for (Log recommendLog:recommendLogs) {
            System.out.println(recommendLog.toString());
        }*/

       /* List<String> lables = iUservice.getLables();

        for (String lable: lables) {
            System.out.println(lable);
        }*/

        List<News> recommendNews = iUservice.getRecommendNews();
        for (News news: recommendNews) {
            System.out.println(news.toString());
        }


    }


}
