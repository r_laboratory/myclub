package com.wfbql.community;

import com.wfbql.community.dao.IUserDao;
import com.wfbql.community.daomain.IUser;
import com.wfbql.community.service.IUservice;
import com.wfbql.community.utils.MD5Utils;
import com.wfbql.community.utils.OSSClientUtil;
import com.wfbql.community.utils.UUIDUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.apache.log4j.Logger;

@SpringBootTest
class CommunityApplicationTests {

    @Autowired
    private OSSClientUtil ossClientUtil;

    @Autowired
    private IUservice iUservice;



    private static Logger logger = Logger.getLogger(CommunityApplicationTests.class);
    @Test
    void testregister(){

      /*  boolean register = iUservice.isRegister("123456");

        IUser register1 = iUservice.register(iUser);
        System.out.println(register);
        System.out.println(register1);*/
        IUser iUser = new IUser(UUIDUtils.getUUID(),"1232456","1111");
        System.out.println(iUservice.register(iUser));

    }



    @Test
    void testlog4j(){

        logger.info("这个是info的信息");
        logger.debug("这个是debug中的信息");
        logger.error("这个是error中的信息");
    }


    @Test
    void testmd5(){

        System.out.println(MD5Utils.md5("1231"));
        //System.out.println(iUservice.login("1233", "1231"));

    }

    @Test
    void testMybatis(){
        System.out.println(iUservice.login("1233", "1231"));

    }



    @Test
    void contextLoads() throws FileNotFoundException {
        // String imgUrl = ossClientUtil.getUrl("t5.jpeg");
        // System.out.println(imgUrl);
        File file = new File("C:\\Users\\wfbql\\Pictures\\Saved Pictures\\t8.jpg");
        String t5 = ossClientUtil.uploadFile2OSS(new FileInputStream(file), "t9.jpg");
        System.out.println(t5);
        System.out.println(ossClientUtil.getUrl("t8.jpg"));
    }

}
