package com.wfbql.community.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {
    //direct模式，直接根据队列名称投递消息
    @Bean
    public Queue logOpQueue(){
        return new Queue("mails");
    }
}
