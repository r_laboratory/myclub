package com.wfbql.community.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;

public class ElasticConfig {

    @Bean
    public RestHighLevelClient restHighLevelClient(){

        RestHighLevelClient  client = new RestHighLevelClient(
                RestClient.builder(new HttpHost("localhost",9200,"http"),
                        new HttpHost("localhost",9201,"http")));
        return client;
    }
}
