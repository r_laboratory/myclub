package com.wfbql.community.config;


import com.wfbql.community.filter.LoginInvocationHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login.html").setViewName("login");
        registry.addViewController("/register.html").setViewName("/register");
        registry.addViewController("/index.html").setViewName("/indexs");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInvocationHandler()).addPathPatterns("/**").excludePathPatterns("/login","/register","/static/**","/getVerifyCode","/tologin","/toregister","/search/**");

    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

    }
}
