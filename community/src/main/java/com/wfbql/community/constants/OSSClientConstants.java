package com.wfbql.community.constants;

/**
 * 阿里云OSS配置
 */
public class OSSClientConstants {

    //地域节点
    public static final String ENDPOINT = "";
    //AccessKey ID
    public static final String ACCESS_KEY_ID  = "";
    //Access Key Secret
    public static final String ACCESS_KEY_SECRET  = "";
    //仓库名称
    public static final String BACKET_NAME  = "communityss";
    //仓库中的某个文件夹
    public static final String FOLDER = "head-portrait/";
}
