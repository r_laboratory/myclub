package com.wfbql.community.controller;

import com.wfbql.community.daomain.IUser;
import com.wfbql.community.daomain.ImgInfo;
import com.wfbql.community.daomain.Log;
import com.wfbql.community.service.LogService;
import com.wfbql.community.utils.DateUtil;
import com.wfbql.community.utils.OSSClientUtil;
import com.wfbql.community.utils.UUIDUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Controller
public class LogController {

    @Autowired
    private LogService logService;



    @RequestMapping("/eitor")
    public String toeitor(){

        return "editbolgs";
    }

    @RequestMapping("/bolgs")
    public String tomybolgs(Model model, HttpSession session){
        //查询出来跳转到我的日志页面
        IUser user = (IUser) session.getAttribute("user");
        //通过user_id查询出该角色的所有日志信息
        List<Log> logs = logService.getLogs(user.getId());
        model.addAttribute("logs",logs);
        return "mybolgs";
    }



    @RequestMapping("/logedite")
    public String edite(@Param("id")String id,Model model){
        //编辑数据,查询出数据后显示到edito中
       Log log =  logService.getLogById(id);
        model.addAttribute("log",log);
        return "editbolg";
    }



    @RequestMapping("/logdelete")
    public String delete(@Param("id") String id, Model model){
        System.out.println("来到了delect中的数据源");
        logService.deleteById(id);
        return "mybolgs";
    }


    @RequestMapping("/logcheck")
    public String check(@Param("id")String id,Model model){
        Log log = logService.getLogById(id);
        model.addAttribute("log",log);
        return "bolg";
    }


    //图片上传
    @RequestMapping("/log/upload")
    @ResponseBody
    public ImgInfo setImgUrl(MultipartFile file, HttpServletResponse response, HttpServletRequest request) throws Exception {

        OSSClientUtil ossClientUtil = new OSSClientUtil();

        String s = ossClientUtil.uploadImg2Oss(file);
        String url = ossClientUtil.getImgUrl(s);

        String value =url;

        String[] values = { value };

        ImgInfo imgInfo = new ImgInfo();
        imgInfo.setError(0);
        imgInfo.setUrl(values);
        return imgInfo;
    }

    @ResponseBody
    @PostMapping("/addlog")
    public String addlog(@Param("ht")String ht ,@Param("themes") String theme,HttpSession session) throws ParseException {
        System.out.println(ht+"====="+theme);
        //获取user_id
        IUser user = (IUser) session.getAttribute("user");
        String id = user.getId();

        Log log = new Log();


        //获取当前系统的日期
        Date date = new Date(System.currentTimeMillis());
        String s = DateUtil.date2String(date);
        log.setDatee(s);
        log.setTheme(theme);
        log.setContent(ht);
        log.setUser_id(id);
        log.setEye(0);
        log.setUsername(user.getUsername());
        log.setId(UUIDUtils.getUUID());


        //查询出来把消息,保存到数据库中去
        logService.insertLog(log);
        return "mybolgs";
    }



}
