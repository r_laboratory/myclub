package com.wfbql.community.controller;

import com.wfbql.community.daomain.IUser;
import com.wfbql.community.daomain.Info;
import com.wfbql.community.service.InfoService;
import com.wfbql.community.service.MailSender;
import com.wfbql.community.utils.OSSClientUtil;
import com.wfbql.community.utils.RedisUtil;
import com.wfbql.community.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.Date;
import java.util.Random;

@Controller
public class InfoController {

    @Autowired
    private InfoService infoService;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private MailSender mailSenders;


    @GetMapping("/info")
    public String info(Model model , HttpSession session){
        //查询显示出info页面,查询info中出显示的信息,通过用户的id查询出来
        //头像的路径我放入了session中了, 所以不用取出头像, 取出其他的即可
        IUser user = (IUser) session.getAttribute("user");
        Info info = infoService.getInfo(user.getId());
        if(info.getEmile()==null){
            info.setEmile("");
        }
        model.addAttribute("info",info);
        return "/info";
    }


    @PostMapping("/makeinfo")
    public String changeimg(String username, String emile, String yzm,String sex, String datee,String evaluation, @RequestParam("sourceId") MultipartFile file, HttpSession session,Model model) throws IOException, ParseException {
        //实现个人信息的保存
        System.out.println(username+"="+emile+"="+sex+"="+datee);

        //获取用户的id值
        IUser user  = (IUser) session.getAttribute("user");
        String id = user.getId();

        //判断验证码是否为空
        if(yzm!=null){
            String mail_id = (String) redisUtil.get(id);
            if(yzm.equals(mail_id)){
                Info info = infoService.makeinfo(username,emile,sex,datee,evaluation,file,session);
            }else {
                //验证码错误
                session.setAttribute("info_msg","邮箱验证码错误!");
            }
        }
        session.setAttribute("info_msg","1");
        //不为空
        //进行保存,然后重定向到页面
        emile="";
        Info info = infoService.makeinfo(username,emile,sex,datee,evaluation,file,session);
        model.addAttribute("info",info);

        return "/info";
    }


    @PostMapping("/sendMail")
    @ResponseBody
    public void sendMail(String emile , HttpSession session){
        IUser user = (IUser) session.getAttribute("user");
        System.out.println(emile+"====="+user.toString());


        //发送一个验证码到具体的邮箱,把验证码存放到redis中去

        //创建一个4位数的验证码
       String s = UUIDUtils.getMailID();

       //把验证邮箱的验证码放入redis中
       redisUtil.set(user.getId(),s);

       //发送邮件
        String html ="您的邮箱验证码为:<h1>"+s+"</h1>";
        //mailSenders.send("2528697821@qq.com","测试邮件","<h1>测试邮件内容</h1>","",""
        mailSenders.send(emile,"绑定邮箱",s,"","");

        //在更新个人信息的时候,查看验证码中是否有值,

    }
}
