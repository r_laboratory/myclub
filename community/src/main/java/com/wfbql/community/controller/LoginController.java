package com.wfbql.community.controller;


import com.wfbql.community.daomain.IUser;
import com.wfbql.community.daomain.Label;
import com.wfbql.community.daomain.Log;
import com.wfbql.community.daomain.News;
import com.wfbql.community.service.IUservice;
import com.wfbql.community.utils.MD5Utils;
import com.wfbql.community.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 进行登录注册功能
 */

@Controller
public class LoginController {

    @Autowired
    private IUservice iUservice;


    @RequestMapping("/login")
    public String tologin(){
        return "login";
    }

    @RequestMapping("/register")
    public String toregister(){
        return "register";
    }

    @RequestMapping("/index")
    public String toindex(){
        return "indexs";
    }


    @RequestMapping("/test")
    public String totest(){
        return "test";
    }
    /**
     * 进行登录操作
     * @return
     */
    @PostMapping("/tologin")
    public String login( @RequestParam("username")String username,@RequestParam("password")String password,  @RequestParam("code")String code,HttpSession session){

        //System.out.println(username+"="+password+"="+code);
        session.setAttribute("msg",1);
        //比较验证码是否相同,相同则进行登录操作,不相同放回错误数据
        if(code.equals(session.getAttribute("verifyCode"))){
            //进行登录操作
            IUser login = iUservice.login(username, MD5Utils.md5(password));
            if(login!=null){
                //把登录的账号密码存到session中
                session.setAttribute("user",login);
                return "redirect:/indexs";
            }else {
                //账号或密码错误
                session.setAttribute("msg","账号或密码错误!");
                return "redirect:/login";
            }
        }else{
            //验证码错误,不能进行登录,放回错误信息
            session.setAttribute("msg","验证码错误!");
            return "redirect:/login";
        }
    }

    /**
     * 进行注册
     * @return
     */
    @RequestMapping("/toregister")
    public String register(@RequestParam String username,@RequestParam("password1") String password,@RequestParam String code,HttpSession session){
        System.out.println(username+"=="+password+"="+code);

        //判断验证码是否正确
        session.setAttribute("msg",1);
        //比较验证码是否相同,相同则进行登录操作,不相同放回错误数据
        if(code.equals(session.getAttribute("verifyCode"))){
            //验证码正确进行注册操作,用户数据加密
            //判断是否有相同的用户名
            boolean flag = iUservice.isRegister(username);
            if(flag){
                //true 没有改用户名,可以进行注册
                IUser iUser = new IUser();
                iUser.setId(UUIDUtils.getUUID());
                iUser.setUsername(username);
                iUser.setPassword(MD5Utils.md5(password));
                //获取注册的用户进行登录
                IUser iUser1  = iUservice.register(iUser);
                //把成功注册的用户存入session中去
                session.setAttribute("user",iUser1);
                return "/index";
            }else {
                    //有相同的用户名,不能注册
                    session.setAttribute("msg","改用户名已被注册不能再注册");
                    return "/register";
            }

        }else{
                //验证码不相同
                session.setAttribute("msg","验证码错误!");
                return "/register";
        }
    }



    //显示主页的所有信息
    //左边的labels , 推荐文章 , 新闻消息
    @GetMapping("/indexs")
    public String leader(Model model , HttpSession session){
        Map<String, Object> map = new HashMap<>();
        //首先把用户名显示在session.getUsername,通过id查询出个人以个人id的账号 ,头像

        IUser iUser = (IUser) session.getAttribute("user");

        String username  =  iUservice.getNameById(iUser.getId());

        //获取头像的路径 , 通过id查询出image_path
        String image_head = iUservice.getImageById(iUser.getId());

        session.setAttribute("username",username);
        session.setAttribute("image_head",image_head);



        //遍历查询出lables标签
        List<Label> labels =  iUservice.getLables();

        session.setAttribute("labels",labels);
        map.put("labels",labels);
        //查询出推荐的内容只查询出五条数据即可 查看数量大于 3000 即可成为热点文章
        //文章的href , 主题 , 作者 , 查看人数 , 创作的时间 , 不需要查询出其内容
        List<Log> logs =  iUservice.getRecommendLogs();
        //map.put("logs",logs);

        model.addAttribute("logs",logs);
        //查询出新闻的热点内容
        List<News>  news = iUservice.getRecommendNews();

        model.addAttribute("news",news);
        model.addAttribute("maps",map);

        //获取拜访人的数量
        //iUservice.getVisiter();

        //查询出访问的人数
        return "/index";
    }

}
