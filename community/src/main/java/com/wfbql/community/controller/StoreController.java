package com.wfbql.community.controller;

import com.wfbql.community.service.ElasticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Controller
public class StoreController {

    @Autowired
    private ElasticService elasticService;


    @RequestMapping("/search")
    public String toss(){
        return "store";
    }



    @GetMapping("/search/{keyword}")
    @ResponseBody
    public boolean tosearch(@PathVariable("keyword") String keyword) throws IOException {
        boolean falg = elasticService.parseContent(keyword);
        return falg;
    }

    @GetMapping("search/{keyword}/{pageNo}/{pageSize}")
    @ResponseBody
    public List<Map<String,Object>> tostore(@PathVariable("keyword") String keyword,@PathVariable("pageNo") int pageNo,@PathVariable("pageSize") int pageSize) throws IOException {

      //  elasticService.parseContent(keyword);

        List<Map<String, Object>> list = elasticService.searchPageAndLight(keyword, pageNo, pageSize);

        return list;
    }
}
