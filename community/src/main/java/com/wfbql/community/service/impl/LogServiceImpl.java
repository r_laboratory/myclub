package com.wfbql.community.service.impl;

import com.wfbql.community.dao.LogDao;
import com.wfbql.community.daomain.Log;
import com.wfbql.community.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional(propagation= Propagation.SUPPORTS,readOnly=true,rollbackFor = Exception.class)
public class LogServiceImpl  implements LogService {

    @Autowired
    private LogDao logDao;

    @Override
    public List<Log> getLogs(String id) {
        List<Log> logs =  logDao.selectLogByUId(id);
        return logs;
    }



    @Override
    public Log getLogById(String id) {
        Log  log = logDao.selectLogById(id);
        return log;
    }

    @Override
    public void deleteById(String id) {
        logDao.deleteById(id);
    }

    @Override
    public void insertLog(Log log) {
        logDao.insertLogById(log);
    }


}
