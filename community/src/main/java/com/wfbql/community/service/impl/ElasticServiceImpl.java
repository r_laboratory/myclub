package com.wfbql.community.service.impl;

import com.alibaba.fastjson.JSON;
import com.wfbql.community.constants.ElasticConstants;
import com.wfbql.community.daomain.Content;
import com.wfbql.community.service.ElasticService;
import com.wfbql.community.utils.HtmlParseUtil;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.Highlighter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class ElasticServiceImpl implements ElasticService {

    @Autowired
    private RestHighLevelClient restHighLevelClient;


    public boolean parseContent(String keyword) throws IOException {
        //把解析的图片信息取出来
        List<Content> contents = new HtmlParseUtil().ParseHtml(keyword);

        BulkRequest bulkRequest = new BulkRequest();
        //设置解析的时间
        bulkRequest.timeout("2m");

        for (int i = 0; i < contents.size(); i++) {
            System.out.println(JSON.toJSONString(contents.get(i)));
            bulkRequest.add(new IndexRequest(ElasticConstants.INDEX)
                    .source(JSON.toJSONString(contents.get(i)), XContentType.JSON));

        }
        BulkResponse bulk = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
        return !bulk.hasFailures();
    }


    @Override
    public List<Map<String, Object>> searchPage(String keyword, int pageNo, int pageSize) throws IOException {
        //条件搜索
        SearchRequest searchRequest = new SearchRequest(ElasticConstants.INDEX);
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();



        MatchQueryBuilder title = QueryBuilders.matchQuery("title", keyword);
        sourceBuilder.query(title);
        sourceBuilder.timeout(new TimeValue(30, TimeUnit.SECONDS));

        sourceBuilder.from(pageNo);
        sourceBuilder.size(pageSize);

        //执行搜索
        searchRequest.source(sourceBuilder);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        ArrayList<Map<String,Object>> list = new ArrayList<>();
        for (SearchHit hit : searchResponse.getHits().getHits()) {
                list.add(hit.getSourceAsMap());
        }
        return list;
    }

    @Override
    public List<Map<String, Object>> searchPageAndLight(String keyword, int pageNo, int pageSize) throws IOException {
        //条件搜索
        SearchRequest searchRequest = new SearchRequest(ElasticConstants.INDEX);
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

        //分页
        sourceBuilder.from(pageNo);
        sourceBuilder.size(pageSize);

        //精确匹配
        MatchQueryBuilder matchQuery = QueryBuilders.matchQuery("title", keyword);
        sourceBuilder.query(matchQuery);
        sourceBuilder.timeout(new TimeValue(30, TimeUnit.SECONDS));

        //高亮
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        highlightBuilder.requireFieldMatch(false); //多个高亮显示
        highlightBuilder.field("title");
        highlightBuilder.preTags("<span style='color:red'>");
        highlightBuilder.postTags("</span>");
        sourceBuilder.highlighter(highlightBuilder);

        //执行搜索
        searchRequest.source(sourceBuilder);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        //解析结果
        ArrayList<Map<String,Object>> list = new ArrayList<>();
        for (SearchHit hit : searchResponse.getHits().getHits()) {

            //解析高亮的字段

            //获取高亮字段
            Map<String, HighlightField> highlightFields = hit.getHighlightFields();

            HighlightField titles = highlightFields.get("title");

            Map<String, Object> sourceAsMap = hit.getSourceAsMap(); //原来的结果

            //解析高亮字段,把原来的字段替换成非高亮的字段
            if(titles!=null){
                Text[] fragments = titles.fragments();
                String n_title = "";
                for (Text fragment : fragments) {
                    n_title += fragment;
                }
                sourceAsMap.put("title",n_title);
            }
        list.add(sourceAsMap);
        }
        return list;
    }

}
