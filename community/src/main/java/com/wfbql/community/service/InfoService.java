package com.wfbql.community.service;

import com.wfbql.community.daomain.Info;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.text.ParseException;

public interface InfoService {


    Info getInfo(String id);


    Info makeinfo(String username, String emile, String sex, String datee,String evaluation, MultipartFile file, HttpSession session) throws ParseException;
}
