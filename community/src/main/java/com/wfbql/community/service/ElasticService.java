package com.wfbql.community.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface ElasticService {

    //把爬取网页中的内容
    public boolean parseContent(String keyword) throws IOException;

    //在es中查询图片
    public List<Map<String,Object>> searchPage (String keyword,int pageNo,int pageSize) throws IOException;

    //搜索高亮
    public List<Map<String,Object>> searchPageAndLight (String keyword,int pageNo,int pageSize) throws IOException;
}
