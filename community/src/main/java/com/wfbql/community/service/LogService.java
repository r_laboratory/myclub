package com.wfbql.community.service;

import com.wfbql.community.daomain.Log;

import java.util.List;

public interface LogService {

    //通过user_id查询出该角色的所有日志信息
    List<Log> getLogs(String id);

    //查询出数据后显示到edito中
    Log getLogById(String id);

    //根据id删除该日志
    void deleteById(String id);


    void insertLog(Log log);
}
