package com.wfbql.community.service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;


/**
 *  这里有两种方案，一种是按我下面给出的代码直接CP就可以，另一种是
 *  @Component
 *  public class XXXSender（名字可以自行定义）{}
 */
@Component("mailSenders")
public class MailSender {
    private final Logger logger = LoggerFactory.getLogger(MailSender.class);
    @Autowired
    AmqpTemplate amqpTemplate;
    @Value("${spring.mail.username}")
    private String from;
    @Value("${spring.mail.personal}")
    private String personal;
    /**
     * TODO: 将邮件信息内容推送至消息队列
     * @param to        收件人
     * @param subject   主题
     * @param content   内容
     * @param fileUrl   附件
     * @param fileName  附件名称
     * @return
     */
    public String send(String to,String subject,String content,String fileUrl,String fileName){
        logger.info("邮件发送时间："+new Date());


        System.out.println("==============================="+to);
        //TODO: 封装邮件消息
        BasePage mail = new BasePage();
        mail.put("from",from);
        mail.put("personal",personal);
        mail.put("to",to);
        mail.put("subject",subject);
        mail.put("content",content);
        mail.put("fileUrl",fileUrl);
        mail.put("fileName",fileName);

        amqpTemplate.convertAndSend("mails",mail);
        return "已成功推送至邮件服务队列";
    }
}
