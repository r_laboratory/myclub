package com.wfbql.community.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.UnsupportedEncodingException;

@Component
public class MailBase {
    @Autowired
    JavaMailSender javaMailSender;

    /**
     * TODO： 发送邮件
     * @param from      发件人
     * @param personal  发件人昵称
     * @param to        收件人
     * @param subject   主题
     * @param content   内容
     * @param fileUrl   附件
     * @param fileName  附件名称
     */
    public void sendSimpleMail(String from,String personal,String to,String subject,String content,String fileUrl,String fileName){
        MimeMessage message = null;
        try {
            message = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(from,personal);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(content, true);
            if (!fileUrl.equals("") && !fileName.equals("")){
                FileSystemResource fileSystemResource=new FileSystemResource(new File(fileUrl));
                helper.addAttachment(fileName,fileSystemResource);
            }
            javaMailSender.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
