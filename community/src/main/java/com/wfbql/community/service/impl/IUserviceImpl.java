package com.wfbql.community.service.impl;

import com.wfbql.community.constants.FaceConstants;
import com.wfbql.community.dao.IUserDao;
import com.wfbql.community.daomain.IUser;
import com.wfbql.community.daomain.Label;
import com.wfbql.community.daomain.Log;
import com.wfbql.community.daomain.News;
import com.wfbql.community.service.IUservice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(propagation= Propagation.SUPPORTS,readOnly=true,rollbackFor = Exception.class)
public class IUserviceImpl implements IUservice {

    @Autowired
    private IUserDao iUserDao;

    @Override
    public IUser login(String username, String password) {

        IUser iUser = iUserDao.selectUser(username, password);
        return iUser;
    }

    @Override
    public boolean isRegister(String username) {
        IUser iUser= iUserDao.selectByUsername(username);
        if(iUser!=null){
            return false;
        }
        return true;
    }

    @Override
    public IUser register(IUser iUser) {
        IUser iUser1 = null;
        try{
            //插入数据后查询数据
            //向信息表中插入数据
            iUserDao.insertUser(iUser);

            //查询出来
            iUser1 = iUserDao.selectByUsername(iUser.getUsername());

            //把头像的信息插入进去
            iUserDao.insertImage(iUser1.getId(),FaceConstants.IMAGE_PATH);
            //插入信息表
            iUserDao.insertInfo(iUser1.getId(),iUser.getUsername());

        }catch (Exception e){
            e.printStackTrace();
        }
        return iUser1;
    }

    @Override
    public String getNameById(String id) {
        String username = iUserDao.selectNameById(id);
        return username;
    }

    @Override
    public String getImageById(String id) {
        String image =  iUserDao.selectImageById(id);
        return image;
    }

    @Override
    public List<Label> getLables() {
        List<Label> lables = iUserDao.selectLables();
        return lables;
    }

    @Override
    public List<Log> getRecommendLogs() {
        List<Log> logs = iUserDao.selectRecommendLogs();
        return logs;
    }

    @Override
    public List<News> getRecommendNews() {
        List<News>   news = iUserDao.selectRecommendNews();
        return news;
    }
}
