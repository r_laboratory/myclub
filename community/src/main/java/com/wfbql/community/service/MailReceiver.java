package com.wfbql.community.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

@Component
@RabbitListener(queues = "mails")
public class MailReceiver {
    private final Logger logger = LoggerFactory.getLogger(MailSender.class);
    @Resource
    MailBase mailBase;

    @RabbitHandler
    public void process(BasePage mail) {
        if(mail.getString("to")!=null){
            System.out.println("+++++++++++++++++++++++++++++"+mail.getString("to"));
            // TODO 开始处理队列中的内容·推送至邮件服务器进行发送
            mailBase.sendSimpleMail(mail.getString("from"),
                    mail.getString("personal"),
                    mail.getString("to"),
                    mail.getString("subject"),
                    mail.getString("content"),
                    mail.getString("fileUrl"),
                    mail.getString("fileName"));
            logger.info("邮件推送时间：" + new Date());
        }

    }
}
