package com.wfbql.community.service;


import com.wfbql.community.daomain.IUser;
import com.wfbql.community.daomain.Label;
import com.wfbql.community.daomain.Log;
import com.wfbql.community.daomain.News;
import org.springframework.stereotype.Service;

import java.util.List;


public interface IUservice {
    //进行登录
    public IUser login(String username, String password);

    //判断用户名是否可用
    boolean isRegister(String username);

    //进行注册
    IUser register(IUser iUser);

    //////////////////////////
    //通过id查询出名称
    String getNameById(String id);

    //通过id查询出头像
    String getImageById(String id);

    //查询出label中的所有标签
    List<Label> getLables();

    //查询出推荐的logs
    List<Log> getRecommendLogs();

    //查询出推荐的news
    List<News> getRecommendNews();
}
