package com.wfbql.community.service.impl;

import com.wfbql.community.dao.InfoDao;
import com.wfbql.community.daomain.IUser;
import com.wfbql.community.daomain.Info;
import com.wfbql.community.service.InfoService;
import com.wfbql.community.utils.DateUtil;
import com.wfbql.community.utils.OSSClientUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.text.ParseException;

@Service
@Transactional(propagation= Propagation.SUPPORTS,readOnly=true,rollbackFor = Exception.class)
public class InfoServiceImpl implements InfoService {

    @Autowired
    private InfoDao infoDao ;

    @Autowired
    private OSSClientUtil ossClientUtil;

    @Override
    public Info getInfo(String id) {

       Info  info = infoDao.selectInfo(id);

        return info;
    }

    @Override
    public Info makeinfo(String username, String emile, String sex, String datee, String evaluation,MultipartFile file, HttpSession session) throws ParseException {
        //如果输入的文件不为空, 则进行头像的保存操作
        IUser user = (IUser) session.getAttribute("user");
        String id = user.getId();

        if(!file.isEmpty()){
            //实现上传图片 , 图片上传到OSS中,然后获取url,存入mysql数据库中去
            String img_name = ossClientUtil.uploadImg2Oss(file);
            String imgUrl = ossClientUtil.getImgUrl(img_name);
            //进行把头像信息保存到数据库中
            infoDao.updateFace(id,imgUrl);
            //把头像的url返回
             session.setAttribute("image_head",imgUrl);
        }
        //构造info对象 日志保存在es中 , bolg  节点中去
        Info info = new Info();
        info.setId(id);
        info.setAlias(username);
        info.setEmile(emile);
        info.setDatee(DateUtil.time2String(datee));
        info.setEvaluation(evaluation);

        //更新数据库,然后查询出来
      //  infoDao.updateInfo1(id,username,emile,DateUtil.time2String(datee),evaluation);
        infoDao.updateInfo(info);

        Info info1 = infoDao.selectInfo(id);

        return info1;
    }
}
