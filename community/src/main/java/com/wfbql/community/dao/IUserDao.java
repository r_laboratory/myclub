package com.wfbql.community.dao;


import com.wfbql.community.daomain.IUser;
import com.wfbql.community.daomain.Label;
import com.wfbql.community.daomain.Log;
import com.wfbql.community.daomain.News;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface IUserDao {

    public IUser selectUser(String username, String password);

    IUser selectByUsername(String username);

    void insertUser(IUser iUser);

    void insertInfo(String id,String username);

    void insertImage(String id,String image);


    ///////
    String selectNameById(String id);

    String selectImageById(String id);

    List<Label> selectLables();

    List<Log> selectRecommendLogs();

    List<News> selectRecommendNews();
}
