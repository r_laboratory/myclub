package com.wfbql.community.dao;

import com.wfbql.community.daomain.Log;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface LogDao {
    public List<Log> selectLogByUId(String id);

    Log selectLogById(String id);

    void deleteById(String id);

    void insertLogById(@Param("log") Log log);
}
