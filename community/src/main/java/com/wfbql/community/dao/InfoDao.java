package com.wfbql.community.dao;

import com.wfbql.community.daomain.Info;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.Date;

@Mapper
public interface InfoDao {
    Info selectInfo(String id);

    void updateFace(String id, String imgUrl);

    void updateInfo(@Param("info") Info info);

    void updateInfo1(String id, String username, String emile, Date time2String, String evaluation);
}
