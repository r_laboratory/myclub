package com.wfbql.community.daomain;

import com.wfbql.community.utils.DateUtil;

import java.text.ParseException;
import java.util.Date;

public class Log {

    private String id;

    private String user_id;

    private String theme;

    private String content;

    private Date datee;

    private Integer eye;

    private String username;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDatee() {
        String s = DateUtil.date2String(this.datee);
        String substring = s.substring(0, 10);
        return substring;
    }

    public void setDatee(String datee) throws ParseException {
        Date date = DateUtil.time2String(datee);
        this.datee = date;
    }

    public Integer getEye() {
        return eye;
    }

    public void setEye(Integer eye) {
        this.eye = eye;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    @Override
    public String toString() {
        return "Log{" +
                "id='" + id + '\'' +
                ", user_id='" + user_id + '\'' +
                ", theme='" + theme + '\'' +
                ", content='" + content + '\'' +
                ", date=" + datee +
                ", eye=" + eye +
                ", user='" + username + '\'' +
                '}';
    }
}
