package com.wfbql.community.daomain;

import java.util.Date;


public class News {

    private  String id ;

    private String theme;

    private String info;

    private Date datee;

    private Integer eye;

    private  String image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Date getDatee() {
        return datee;
    }

    public void setDatee(Date datee) {
        this.datee = datee;
    }

    public Integer getEye() {
        return eye;
    }

    public void setEye(Integer eye) {
        this.eye = eye;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public News() {
    }

    public News(String id, String theme, String info, Date date, Integer eye, String url) {
        this.id = id;
        this.theme = theme;
        this.info = info;
        this.datee = date;
        this.eye = eye;
        this.image = url;
    }

    @Override
    public String toString() {
        return "News{" +
                "id='" + id + '\'' +
                ", theme='" + theme + '\'' +
                ", info='" + info + '\'' +
                ", date=" + datee +
                ", eye=" + eye +
                ", url='" + image + '\'' +
                '}';
    }
}
