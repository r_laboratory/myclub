package com.wfbql.community.daomain;

public class Content {


    private String img;

    private String price;

    private String publishing;

    private String title;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPublishing() {
        return publishing;
    }

    public void setPublishing(String publishing) {
        this.publishing = publishing;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public Content(String img, String price, String publishing, String title) {
        this.img = img;
        this.price = price;
        this.publishing = publishing;
        this.title = title;
    }

    @Override
    public String toString() {
        return "Image{" +
                "img='" + img + '\'' +
                ", price='" + price + '\'' +
                ", publishing='" + publishing + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
