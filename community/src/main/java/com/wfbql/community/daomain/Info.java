package com.wfbql.community.daomain;

import com.wfbql.community.utils.DateUtil;

import java.util.Date;

public class Info {

    private String id;

    private String alias;

    private String emile;

    private String sex;

    private Date datee;

    private String ineresting;

    private String evaluation;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getEmile() {
        return emile;
    }

    public void setEmile(String emile) {
        this.emile = emile;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getDatee() {
     String s = DateUtil.date2String(this.datee);
     String substring = s.substring(0, 10);
     return substring;
    }

    public void setDatee(Date datee) {
        this.datee = datee;
    }

    public String getIneresting() {
        return ineresting;
    }

    public void setIneresting(String ineresting) {
        this.ineresting = ineresting;
    }

    public String getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(String evaluation) {
        this.evaluation = evaluation;
    }

    @Override
    public String toString() {
        return "Info{" +
                "id='" + id + '\'' +
                ", alias='" + alias + '\'' +
                ", emile='" + emile + '\'' +
                ", sex='" + sex + '\'' +
                ", datee=" + datee +
                ", ineresting='" + ineresting + '\'' +
                ", evaluation='" + evaluation + '\'' +
                '}';
    }
}
