package com.wfbql.community.utils;
import java.util.Random;
import java.util.UUID;

/**
 * 生成随机字符串的工具类
 */
public class UUIDUtils {

    public static String getUUID(){
        return UUID.randomUUID().toString().replace("-", "");
    }

    public static String getMailID(){
        Random random = new Random();
        String s="";
        for (int i = 0; i < 4; i++) {
            int j = random.nextInt(10);
            s+= j;
        }
        System.out.println(s);
        return s;
    }
}
