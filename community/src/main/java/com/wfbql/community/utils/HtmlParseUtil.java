package com.wfbql.community.utils;

import com.wfbql.community.daomain.Content;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class HtmlParseUtil {

    public static List<Content> ParseHtml(String keyword) throws IOException {

        List<Content> images = new ArrayList<>();

        //创建一个解析网页的路径
        String url  = "https://search.jd.com/Search?keyword="+keyword;
        //获取网页的文档对象
        Document parse = Jsoup.parse(new URL(url), 30000);
        Element elementById = parse.getElementById("J_goodsList");
        Elements li = elementById.getElementsByTag("li");

        for (Element el: li) {
            String img  =  el.getElementsByTag("img").eq(0).attr("src");
            String price = el.getElementsByClass("p-price").eq(0).text().replaceAll("￥","");
            String publish = el.getElementsByClass("curr-shop hd-shopname").eq(0).attr("title");
            String s  =  el.getElementsByClass("promo-words").eq(0).text();
            String em = el.getElementsByClass("p-name").eq(0).text();
            images.add(new Content(img,price,publish,em.replaceAll(s,"")));
        }
        return images;
    }

}
