/*
 Navicat Premium Data Transfer

 Source Server         : wfbql
 Source Server Type    : MySQL
 Source Server Version : 50562
 Source Host           : localhost:3306
 Source Schema         : communityss

 Target Server Type    : MySQL
 Target Server Version : 50562
 File Encoding         : 65001

 Date: 30/05/2020 17:08:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for face
-- ----------------------------
DROP TABLE IF EXISTS `face`;
CREATE TABLE `face`  (
  `id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `face_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of face
-- ----------------------------
INSERT INTO `face` VALUES ('1', 'http://communityss.oss-cn-shenzhen.aliyuncs.com/head-portrait/1590634578948.jpg?Expires=1905994571&OSSAccessKeyId=LTAI4G5RiYkNNwUa9BViB1xc&Signature=oR066aQNBNXa106vl%2B9dsLDmQoU%3D');
INSERT INTO `face` VALUES ('2e6e724ca06e4b669cb4834fb3991104', 'wfbql');
INSERT INTO `face` VALUES ('33c3b6f2e02f4bea8d9c1d1121faadaf', 'http://communityss.oss-cn-shenzhen.aliyuncs.com/head-portrait/1590634578948.jpg?Expires=1905994571&OSSAccessKeyId=LTAI4G5RiYkNNwUa9BViB1xc&Signature=oR066aQNBNXa106vl%2B9dsLDmQoU%3D');
INSERT INTO `face` VALUES ('87e48785b38642f1959c1e520a3256cd', 'https://communityss.oss-cn-shenzhen.aliyuncs.com/head-portrait/user1-128x128.jpg?OSSAccessKeyId=LTAI4G5RiYkNNwUa9BViB1xc&Expires=37590202411&Signature=YS1zK8Ejbt9mX%2FKkBqDumFYay0Y%3D');
INSERT INTO `face` VALUES ('d6966cf4a70f4d7f876b4ce0cdf64f41', 'https://communityss.oss-cn-shenzhen.aliyuncs.com/head-portrait/user1-128x128.jpg?OSSAccessKeyId=LTAI4G5RiYkNNwUa9BViB1xc&Expires=37590202411&Signature=YS1zK8Ejbt9mX%2FKkBqDumFYay0Y%3D');

-- ----------------------------
-- Table structure for info
-- ----------------------------
DROP TABLE IF EXISTS `info`;
CREATE TABLE `info`  (
  `id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `alias` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `emile` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sex` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `datee` date NULL DEFAULT NULL,
  `interesting` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `evaluation` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  CONSTRAINT `FK_Reference_4` FOREIGN KEY (`id`) REFERENCES `face` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Reference_5` FOREIGN KEY (`id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of info
-- ----------------------------
INSERT INTO `info` VALUES ('1', '张三三', NULL, NULL, '2017-12-03', '', '本人性格偏内向、细致，为人随和，易于沟通，能够比较轻易地融入工作群体，专业知识较 扎实，善于思考，学习能力强，对新事物接受能力快，对感兴趣的方面会花时间研究琢磨， 工作主动性高，做事认真负责。本人性格稳重踏实，工作细心、耐心谨慎，具有较强的亲和力和团队协作精神；能服从组织，领导安排， 维护.');
INSERT INTO `info` VALUES ('33c3b6f2e02f4bea8d9c1d1121faadaf', 'admin', '', 'GG', '2017-12-07', '', '');
INSERT INTO `info` VALUES ('87e48785b38642f1959c1e520a3256cd', 'lisi', '', NULL, '2017-01-05', '', 'evaluation');
INSERT INTO `info` VALUES ('d6966cf4a70f4d7f876b4ce0cdf64f41', '王五', '', 'GG', '2017-12-07', '', '本人性格偏内向、细致，为人随和，易于沟通，能够比较轻易地融入工作群体，专业知识较 扎实，善于思考，学习能力强，对新事物接受能力快，对感兴趣的方面会花时间研究琢磨， 工作主动性高，做事认真负责。本人性格稳重踏实，工作细心、耐心谨慎，具有较强的亲和力和团队协作精神；能服从组织，领导安排， 维护.');

-- ----------------------------
-- Table structure for log
-- ----------------------------
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log`  (
  `id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `theme` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `datee` date NULL DEFAULT NULL,
  `eye` int(11) NULL DEFAULT NULL,
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_Reference_3`(`user_id`) USING BTREE,
  CONSTRAINT `FK_Reference_3` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of log
-- ----------------------------
INSERT INTO `log` VALUES ('1', '1', 'springboot实现多个bug', '都好久撒个谎撒即可还是干哈角色的干哈角色的很骄傲的鬼屋的合适的空间发空间发的啥课件飞洒皇帝文化课撒旦画', '2020-05-22', 12732, '李四');
INSERT INTO `log` VALUES ('2a21fa0cdbad48f284a8e0561ec8dbb0', '1', '这个是张三', '这个是李四', '2016-12-01', 0, '王二麻子');
INSERT INTO `log` VALUES ('51dd704edc614d54bc3d2e1f3e614b05', '1', NULL, '<p></p><p>dsadsadadas这个是main</p>', '2020-05-28', 0, '1233');

-- ----------------------------
-- Table structure for main_label
-- ----------------------------
DROP TABLE IF EXISTS `main_label`;
CREATE TABLE `main_label`  (
  `id` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `label` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of main_label
-- ----------------------------
INSERT INTO `main_label` VALUES ('1', '计算机');
INSERT INTO `main_label` VALUES ('10', '科创');
INSERT INTO `main_label` VALUES ('11', '交友');
INSERT INTO `main_label` VALUES ('12', '琴艺');
INSERT INTO `main_label` VALUES ('2', '古典文学');
INSERT INTO `main_label` VALUES ('3', '书法');
INSERT INTO `main_label` VALUES ('4', '摄影');
INSERT INTO `main_label` VALUES ('5', '骑行');
INSERT INTO `main_label` VALUES ('6', '旅游');
INSERT INTO `main_label` VALUES ('7', '数学竞赛');
INSERT INTO `main_label` VALUES ('8', '武术');
INSERT INTO `main_label` VALUES ('9', '跨文化');

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news`  (
  `id` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `theme` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `info` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `datee` date NULL DEFAULT NULL,
  `eye` int(11) NULL DEFAULT NULL,
  `image` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES ('1222', 'springboot实现多个bugsadsaddasdsadas', '搭嘎登记噶事搭嘎实打实看的哈是否健康gas会计法gas空间发收款时间放假撒看哈数据库', '2020-04-29', 12322, 'https://communityss.oss-cn-shenzhen.aliyuncs.com/head-portrait/default-150x150.png?OSSAccessKeyId=LTAI4G5RiYkNNwUa9BViB1xc&Expires=5190211796&Signature=0SKx3er%2BIi9wv8kIvSRaPFHIKdQ%3D');
INSERT INTO `news` VALUES ('123', 'css如何显示出多微博框', '过段时间搭嘎防水防汗发货时间发过的合法的数据法国的双方各得时光飞逝分公司接电话分公司将覆盖的说法过段时间贵妃醉酒第三个回复', '2020-05-23', 25684, 'https://communityss.oss-cn-shenzhen.aliyuncs.com/head-portrait/default-150x150.png?OSSAccessKeyId=LTAI4G5RiYkNNwUa9BViB1xc&Expires=5190211796&Signature=0SKx3er%2BIi9wv8kIvSRaPFHIKdQ%3D');
INSERT INTO `news` VALUES ('dsa', 'jdk源码级报错,及解决方法gashdgsahjfgdsjhfdsgjh', 'dasdad', '2020-05-22', 12321, 'https://communityss.oss-cn-shenzhen.aliyuncs.com/head-portrait/default-150x150.png?OSSAccessKeyId=LTAI4G5RiYkNNwUa9BViB1xc&Expires=5190211796&Signature=0SKx3er%2BIi9wv8kIvSRaPFHIKdQ%3D');
INSERT INTO `news` VALUES ('dsas', 'springboot整合', '法师打发坚实的噶十多个挥洒的宫颈癌是的哈大', '2020-05-01', 23432, 'https://communityss.oss-cn-shenzhen.aliyuncs.com/head-portrait/default-150x150.png?OSSAccessKeyId=LTAI4G5RiYkNNwUa9BViB1xc&Expires=5190211796&Signature=0SKx3er%2BIi9wv8kIvSRaPFHIKdQ%3D');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '1233', '6C14DA109E294D1E8155BE8AA4B1CE8E');
INSERT INTO `user` VALUES ('2e6e724ca06e4b669cb4834fb3991104', '123456', '1111');
INSERT INTO `user` VALUES ('33c3b6f2e02f4bea8d9c1d1121faadaf', 'admin', '21232F297A57A5A743894A0E4A801FC3');
INSERT INTO `user` VALUES ('87e48785b38642f1959c1e520a3256cd', '111', '698D51A19D8A121CE581499D7B701668');
INSERT INTO `user` VALUES ('d6966cf4a70f4d7f876b4ce0cdf64f41', '1232456', '1111');

-- ----------------------------
-- Table structure for visiter
-- ----------------------------
DROP TABLE IF EXISTS `visiter`;
CREATE TABLE `visiter`  (
  `id` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `num` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
